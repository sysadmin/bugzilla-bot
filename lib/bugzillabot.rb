# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require_relative 'bugzillabot/bug'
require_relative 'bugzillabot/config'
require_relative 'bugzillabot/connection'
require_relative 'bugzillabot/version'

# Module for bugzilla API client.
module Bugzillabot
  module_function

  def config
    @config ||= Config.new
  end
end
