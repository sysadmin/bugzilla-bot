# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2018-2022 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Nate Graham <nate.org>

require 'pp'
require 'date'
require 'logger'
require 'time'

require_relative '../bugzillabot'
require_relative '../constants'
require_relative '../eoltemplate'
require_relative '../monkey'

module Runner
  class Wishlist
    def self.run(logger:)
      last_change = (DateTime.now.new_offset(0) - 3/24.0).to_datetime.iso8601
      to_change = []
      %w[[wish] [feature] [request] feature\ request wishlist].each do |trigger_word|
        Bugzillabot::Bug.search(status: 'UNCONFIRMED', summary: trigger_word, last_change_time: last_change) do |bug|
          next if bug.product == 'krita'

          if bug.comments.size > 1
            logger.info "Bug #{bug.id} has multiple comments -> ignoring"
            next
          end
          if bug.severity == 'wishlist'
            logger.info "Bug #{bug.id} already is marked wishlist -> ignoring"
            next
          end

          logger.warn "changing #{bug.id} to wishlist"
          to_change << bug
        end
      end

      to_change.each do |bug|
        bug.update(ids: bug.id, severity: 'wishlist')
      end
    end
  end
end
