# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require 'date'

unless ENV['PRODUCTION']
  # Monkey patch for testing! bugtest has an old db dump and it's not trivial
  # to refresh these, so instead opt for client side mangling of #today
  class Date
    class << self
      def today
        Date.parse('2017-05-30')
      end
    end
  end

  # Monkey patch for testing!
  class DateTime
    class << self
      def now
        DateTime.parse('2017-05-30T00:00:00')
      end
    end
  end
end
