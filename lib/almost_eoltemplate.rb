# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

require 'erb'
require 'ostruct'

class AlmostEOLTemplate
  attr_reader :data
  alias to_s data

  def initialize(**shackles)
    @data = File.read(File.join(__dir__, '../data/almost-eol.txt.erb'))
    @data = ERB.new(@data).result(OpenStruct.new(shackles).instance_eval { binding })
  end
end
