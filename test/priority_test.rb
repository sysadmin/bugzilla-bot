# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

require_relative 'test_helper'

require 'modules/priority'

class PriorityTest < Minitest::Test

  def test_bump
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=0')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/get.json"))
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=8')
      .to_return(status: 200, body: JSON.generate({ bugs: [], faults: [] }))
    stub_request(:get, 'https://localhost/rest/bug/283477/comment?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/comment.json"))
    stub_request(:get, 'https://localhost/rest/bug/283477/history?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/history-without-bump.json"))

    stub_request(:get, 'https://localhost/rest/bug/330431?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/330431/get.json"))

    stub_request(:get, 'https://localhost/rest/bug/336802?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/336802/get.json"))

    stub_request(:get, 'https://localhost/rest/bug/336842?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/336842/get.json"))

    stub_request(:get, 'https://localhost/rest/bug/341767?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/341767/get.json"))

    stub_request(:get, 'https://localhost/rest/bug/347105?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/347105/get.json"))

    put_request = stub_request(:put, 'https://localhost/rest/bug/283477?Bugzilla_api_key=testing-api-key')
                  .to_return(status: 200, body: JSON.generate({}))

    Runner::Priority.run(logger: Logger.new($stdout))

    assert_requested(put_request)
  end
  def test_no_override_human
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=0')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/get.json"))
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=8')
      .to_return(status: 200, body: JSON.generate({ bugs: [], faults: [] }))
    stub_request(:get, 'https://localhost/rest/bug/283477/comment?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/comment.json"))
    stub_request(:get, 'https://localhost/rest/bug/283477/history?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/history.json"))

    stub_request(:get, 'https://localhost/rest/bug/330431?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/330431/get.json"))

    stub_request(:get, 'https://localhost/rest/bug/336802?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/336802/get.json"))

    stub_request(:get, 'https://localhost/rest/bug/336842?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/336842/get.json"))

    stub_request(:get, 'https://localhost/rest/bug/341767?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/341767/get.json"))

    stub_request(:get, 'https://localhost/rest/bug/347105?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/347105/get.json"))

    put_request = stub_request(:put, 'https://localhost/rest/bug/283477?Bugzilla_api_key=testing-api-key')
                  .to_return(status: 200, body: JSON.generate({}))

    Runner::Priority.run(logger: Logger.new($stdout))

    assert_not_requested(put_request)
  end
end
